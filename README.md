ermr
=====

An Erlang wrapper around RMR using the Erlang port driver API to communicate with a C process.   

Currently, the port driver is using `socket` instead of the RMR library as a proof of concept. 

To test it, run a socket server listening on port `4040` in your localhost. Then, while running ermr in an erlang shell (see below), run `ermr:start()`. This should automatically make a connection to the server if it worked. Running `ermr:send(binary)` will send a binary payload to the connected socket. Any incoming payloads will currently be printed to the console once they come in. 

Future Work
-----
This version of the port driver was built with the assumption that we can run a second thread that will listen for incoming messages. Right now, the recieve thread is working properly (and miraculously), but sending is not working. This will take some debugging to sort out bur it's hopefully just a dumb mistake. If we do get it working, though, it should be easy to drop-in replace the socket with an an RMR connection. On the other hand, if it turns out that a port driver can only run a single thread, we will need to re-think the solution. 

Requirements 
-----
- Erlang
- rebar3
- C compiler

Build
-----

    $ rebar3 compile

Run in shell
-----

    $ rebar3 shell
    > ermr:start().
    > ermr:send("hello world").
