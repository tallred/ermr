-module(ermr).

% ERMR - Erlang library for communicating with RMR using the Erlang port driver API

-export([start/0, stop/0]).
-export([send/1, init/0]).

-define(SHLIB, "ermr_drv").
-define(LIBDIR, "./priv").

-define(ERMR_CONNECT, 1).
-define(ERMR_SEND, 2).

% start/0 - Entry point for the library
% loads the port driver "./priv/ermr_drv.so" which will run in a different process
% spawns a new process that will call init/0 and start listening for subsequent commands 
start() ->
    case erl_ddll:load_driver(?LIBDIR, ?SHLIB) of
        ok                      -> ok;
        {error, already_loaded} -> ok;
        {error, Reason}         -> exit({error, Reason})
    end,
    spawn(?MODULE, init, []).

% init/0 - called from start/0 after the new process is spawned
% this will register 'ermr' to the new process, open the c driver port to get a handle (Port) and start the listening loop
init() ->
    register(ermr, self()),
    Port = open_port({spawn, ?SHLIB}, [binary]),
    % port_control will call ermr_control in the c driver
    % this call will send the connect command to the driver to establish the initial connection
    port_control(Port, ?ERMR_CONNECT, ""),
    loop(Port).

% stop/0 - sends a stop command to the running process which is handled in loop/1
stop() ->
    ermr ! stop.

% send/1 - send a binary over the connection
send(Bin) ->
    call_port({?ERMR_SEND, Bin}).

% call_port/1 - sends a process message to self() which is handled in loop/1
call_port(Msg) ->
    ermr ! {call, self(), Msg},
    ok.

% handle_recv/1 - called from loop/1, handles incomming messages from port driver
% right now it simply writes the payload to the console, but in the future this can do anything
handle_recv(Data) ->
    io:write(Data).

% loop/1 - awaits incoming messages from either API calls or the c port driver
loop(Port) ->
    receive
        % handle an outgoing message (such as 'send') from an API user
        {call, _Caller, {Cmd, Bin}} ->
            % port_control calls ermr_control on the c side
            % the Cmd is an integer and Bin is a binary payload
            port_control(Port, Cmd, Bin),
            % continue listening for new connections
            loop(Port);

        % handle an incoming message from the port
        % the port should have a thread listening for incoming messages and they will be sent here on arival 
        {Port, {data, Data}} ->
            handle_recv(Data),
            loop(Port);
        
        % handle the stop command. TODO: kill the current ermr process
        stop ->
            port_close(Port);

        % handle situations where the port sends an error and crashes 
        {'EXIT', Port, Reason} ->
            io:format("~p ~n", [Reason]),
            exit(port_terminated)
    end.