/* ERMR Driver 
   An erlang port driver for communcation using RMR
*/
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <erl_driver.h>
#include <ei.h>

#define PORT 4040
#define MAXMSG 512

// Different commands that can come from erlange
// These should match the commands in ermr.erl
#define ERMR_CONNECT 1
#define ERMR_SEND 2

// ErmrData is a struct that holds all the information about the running port driver (like a context)
// Right now, it holds a socket handle, a write pipe, read pipe, thread handle, and a "listening" boolean
// In the future, the socket will be replaced with what ever contextual information is needed for a running RMR process but the others will remain
typedef struct
{
    // Port handle for communicating with Erlang process
    ErlDrvPort port;
    int sock;
    int write_pipe;
    int read_pipe;
    int listening;
    pthread_t thread_id;
} ErmrData;


static char* get_s(const char* buf, int len)
{
    char* result;
    if (len < 1 || len > 1000) return NULL;
    result = driver_alloc(len+1);
    memcpy(result, buf, len);
    result[len] = '\0';
    return result;
}

void* recieve_loop(void* p){
    ErmrData* data = (ErmrData *) p;
    int nbytes;  
    char buffer[MAXMSG];
    FILE *stream;

    while(data->listening) {
        nbytes = read(data->sock, buffer, MAXMSG);
        if (nbytes > 1) {
            stream = fdopen(data->write_pipe, "w");
            fprintf(stream, buffer);
            fclose(stream);
        }
    }

}

static int do_connect(ErmrData* data) {
    // Socket setup
    // TODO: this was originally done with sockets as an experiement. All of this should be replaced with RMR setup code
    int sock = 0;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }
    // Set socket
    // This attaches the socket to the current context data
    data->sock = sock;
    data->listening = 1;

    // Pipe setup
    // We will use pipes to communicate incoming data to the erlang process
    // The reason we do this is because erlang port drivers can register file descriptors to react to with a callback when data comes through them
    // We will register these pipes to call ermr_ready_input when a payload comes in
    // (The reason we're not registering sock is because RMR will not give us access to its own FDs. So we're using a pipe and a listening thread instead)
    int comm_pipe[2];

    if (pipe (comm_pipe))
    {
      fprintf (stderr, "Pipe failed.\n");
      return -1;
    }

    data->read_pipe = comm_pipe[0];
    data->write_pipe = comm_pipe[1];

    // This registers the read_pipe to trigger ermr_ready input when data is ready to be recieved from it
    driver_select(data->port, (ErlDrvEvent)data->read_pipe, DO_READ, 1);

    // Thread start
    // We will start a new thread that will spin and listen for new incoming messages
    pthread_create(&data->thread_id, NULL, recieve_loop, data);

    return 0;
}

// Sends a binary across the connection ( currently not working :/ )
static int do_send(const char* msg, ErmrData* data) {
    return send(data->sock, msg, strlen(msg), 0);
}

// Port Open Callback
static ErlDrvData ermr_start(ErlDrvPort port, char *buff)
{
    // Allocate memory for ErmrData structure
    ErmrData *d = (ErmrData *)driver_alloc(sizeof(ErmrData));
    // Set port
    d->port = port;
    // Return data structure
    return (ErlDrvData)d;
}

// Port Close Callback
static void ermr_stop(ErlDrvData drv_data)
{
    ErmrData *d = (ErmrData *)drv_data;
    // Free memory
    driver_free((char *)drv_data);
}

// Callback for incoming message
static void ermr_ready_input(ErlDrvData drv_data, ErlDrvEvent event)
{
    ErmrData* data = (ErmrData*) drv_data;
    int pipe = data->read_pipe;
    char buf[MAXMSG];
    // Create a new output buffer
    // This is what will be recieved on the erlang side
    ei_x_buff x;
    ei_x_new_with_version(&x);
    int numRead;

    numRead = read(pipe, buf, MAXMSG);
    if (numRead > 0) {
        // Encode a string to the output buffer
        ei_x_encode_string(&x, buf);
        // Write the output buffer out to erlang
        // This will trigger the {Port, {data, Data}} arm of the recieve in loop/1 
        driver_output(data->port, x.buff, x.index);
    }

    ei_x_free(&x);

}

static void ermr_ready_output(ErlDrvData drv_data, ErlDrvEvent event)
{
    // TODO
}

// Control callback. Called when erlang calls port_control
static ErlDrvSSizeT ermr_control(ErlDrvData drv_data, unsigned int command,
                                 char *buf, ErlDrvSizeT len, char **rbuf, ErlDrvSizeT rlen)
{
    ErmrData *d = (ErmrData *)drv_data;
    // Input binary
    char* s = get_s(buf, len);  
    // return code
    int r;
    switch (command)
    {
    case ERMR_CONNECT:
        r = do_connect(d);
        break;
    case ERMR_SEND:
        r = do_send(s, d);
        break;
    default:
        return (ErlDrvSSizeT)ERL_DRV_ERROR_BADARG;
    }
    driver_free(s);
    return 0;
}

ErlDrvEntry drv_entry = {
    NULL,                           /* F_PTR init, called when driver is loaded */
    ermr_start,                     /* L_PTR start,  called when port is opened */
    ermr_stop,                      /* F_PTR stop,   called when port is closed */
    NULL,                           /* F_PTR output, called when erlang has sent */
    ermr_ready_input,               /* F_PTR ready_input,  called when input descriptor ready */
    ermr_ready_output,              /* F_PTR ready_output, called when output descriptor ready */
    "ermr_drv",                     /* char *driver_name, the argument to open_port */
    NULL,                           /* F_PTR finish, called when unloaded */
    NULL,                           /* void *handle, Reserved by VM */
    ermr_control,                   /* F_PTR control, port_command callback */
    NULL,                           /* F_PTR timeout, reserved */
    NULL,                           /* F_PTR outputv, reserved */
    NULL,                           /* F_PTR ready_async, only for async drivers */
    NULL,                           /* F_PTR flush, called when port is about
                                       to be closed, but there is data in driver
                                       queue */
    NULL,                           /* F_PTR call, much like control, sync call to driver */
    NULL,                           /* F_PTR event, called when an event selected
                                       by driver_event() occurs. */
    ERL_DRV_EXTENDED_MARKER,        /* int extended marker, Should always be
                                       set to indicate driver versioning */
    ERL_DRV_EXTENDED_MAJOR_VERSION, /* int major_version, should always be
                                      set to this value */
    ERL_DRV_EXTENDED_MINOR_VERSION, /* int minor_version, should always be
                                       set to this value */
    0,                              /* int driver_flags, see documentation */
    NULL,                           /* void *handle2, reserved for VM use */
    NULL,                           /* F_PTR process_exit, called when a
                                       monitored process dies */
    NULL                            /* F_PTR stop_select, called to close an
                                       event object */
};
// The driver structure is filled with the driver name and function pointer. It uses the driver structure and contains the header file erl_driver.h.
DRIVER_INIT(ermr_drv) /* must match name in driver_entry */
{
    return &drv_entry;
}